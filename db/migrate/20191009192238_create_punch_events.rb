class CreatePunchEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :punch_events do |t|
      t.bigint :user_id, :null => false
      t.datetime :check_in, :null => false
      t.datetime :check_out

      t.timestamps
    end
    
    add_foreign_key :punch_events, :users
  end
end

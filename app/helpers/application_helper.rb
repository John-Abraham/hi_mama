module ApplicationHelper

  def get_alert_class_name(key)
    case key.upcase
    when "NOTICE"
      "success"
    when "ALERT"
      "warning"
    end
  end

  def pretty_print_time(datetime)
    datetime.strftime("%H:%M") if datetime
  end

  def pretty_print_date(datetime)
    datetime.strftime("%d %b") if datetime
  end

  def pretty_print_datetime(datetime)
    datetime.strftime("%d %b, %H:%M") if datetime
  end
end

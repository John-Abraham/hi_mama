class TeachersController < ApplicationController

  before_action :validate_user?, :except => [:index]
  
   def index
     begin       
       if request.post?
         @user = User.get_user(params['search_name'])

         if @user
           session[:current_user_id] = @user.id
           redirect_to "/teachers/show"
         end

         flash[:alert] = "No user found! Please enter a valid name." if @user.nil?
       end
     rescue Exception => e
       # Log the error, notify team and render something went wrong page with approproate message
     end
   end

   def show     
   end

       
   def check_in
     begin
       @user.check_in
       flash[:notice] = "You have successfully checked IN."

       redirect_to "/teachers/show"
     rescue CustomError => e
       flash[:alert] = e.message
       redirect_to "/teachers/show"       
     rescue Exception => e       
       # Log the error, notify team and render something went wrong page with approproate message.
       render :plain => e.message
     end
   end

   def check_out
     begin
       @user.check_out
       flash[:notice] = "You have successfully checked-out. See you soon again!"
       
       redirect_to "/index"
     rescue CustomError => e
       flash[:alert] = e.message
       redirect_to "/teachers/show"       
     rescue Exception => e
       # Log the error, notify team and render something went wrong page with approproate message
       render :plain => e.message
     end
   end
end

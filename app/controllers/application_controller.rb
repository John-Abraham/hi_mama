class ApplicationController < ActionController::Base

  skip_before_action :verify_authenticity_token

  def validate_user?    
    @user = User.find(session[:current_user_id])
  end
  
end

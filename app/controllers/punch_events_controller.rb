class PunchEventsController < ApplicationController

  before_action :validate_user?
  
  def list
    @events = PunchEvent.where("user_id = ?",@user.id).order("check_in desc")
  end
  
  def edit
    begin
      @event = PunchEvent.where("id = ? AND user_id = ?",params['id'],@user.id)[0]
    
      if request.post?
        puts "in edit save event."
        @event.save_changes(get_punch_edit_hash)
        flash[:notice] = "Record successfully saved."
        
        redirect_to "/punch_events/list"
      end
    rescue CustomError => e
      flash[:alert] = e.message
    rescue Exception => e
      puts e # This line will not go to production.
      flash[:alert] = "We are sorry something went wrong. Do not worry, we are on it!"
    end
  end

  def destory
    @event = PunchEvent.where("id = ? AND user_id = ?",params['id'],@user.id)[0]

    if @event
      @event.delete
      head :ok
    end
  end

  private
  def get_punch_edit_hash
    {'check_in' => params['check_in'],
     'check_out' => params['check_out']}
  end
end

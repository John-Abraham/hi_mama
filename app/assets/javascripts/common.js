function goBack() {
  window.history.back();
}

function delete_event(id) {
    if (confirm("Are you sure you want to delete this record?"))
    {
	$.ajax({
	    url: '/punch_events/'+id,
	    type: 'DELETE',
	    success: function(result) {
		$('#event_'+id).remove();
	    }
	});
    }
}

function submitPunchEvents()
{
    $("#check_in").val($("#check_in_display").val());
    $("#check_out").val($("#check_out_display").val());
    $("#punch_edit").submit();
}

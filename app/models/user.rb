class User < ApplicationRecord

  def self.get_user(name)
    User.where("name = ?",name)[0]
  end

  def already_checked_in?
    self.status == 'IN'
  end

  def already_checked_out?
    !already_checked_in? 
  end
    
  def last_check_in
    Time.now
  end

  def last_punch_event
    PunchEvent.where("user_id = ? AND check_out IS NULL",self.id).last
  end
  
  def check_in
    raise CustomError, "User is already checked in." if self.already_checked_in?
    
    User.transaction do
      PunchEvent.create({check_in: Time.now, user_id: self.id})
      count = ActiveRecord::Base.connection.update("UPDATE users SET status = 'IN' WHERE status = 'OUT' AND id = #{self.id}")

      # Following can only happen if there was a cuncurrency issue. So taking a pre-causion.
      raise "Error in adding check_in event. ERROR code: #XXXX:" if count == 0
    end
  end

  def check_out
    raise "User is already checked out!" if self.already_checked_out?
    
    User.transaction do
      # If for some reason, the below return more than one record then some is wrong. Might need to revist that.
      event = PunchEvent.where("user_id = ? AND check_out IS NULL",self.id).last
      event.check_out = Time.now
      event.save
      
      count = ActiveRecord::Base.connection.update("UPDATE users SET status = 'OUT' WHERE status = 'IN' AND id = #{self.id}")

      # Following can only happen if there was a cuncurrency issue. So taking a pre-causion.
      raise "Error in updating check_out event. ERROR code: #XXXX:" if count == 0
    end
  end
end

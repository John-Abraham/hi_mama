class PunchEvent < ApplicationRecord

  def save_changes(input_hash)    
    self.check_in = input_hash['check_in']
    self.check_out = input_hash['check_out']

    raise CustomError, "Checkout datetime must be greater than checkin datetime." if self.check_in >= self.check_out
    
    self.save    
  end
end

Rails.application.routes.draw do
  get '/index', to: 'teachers#index'
  post '/index', to: 'teachers#index'

  get '/teachers/show', to: 'teachers#show'
  get '/teachers/check_in', to: 'teachers#check_in'
  get '/teachers/check_out', to: 'teachers#check_out'

  get '/punch_events/list', to: 'punch_events#list'
  delete '/punch_events/:id', to: 'punch_events#destory'
  get '/punch_events/:id/edit', to: 'punch_events#edit'
  post '/punch_events/:id/edit', to: 'punch_events#edit'
end
